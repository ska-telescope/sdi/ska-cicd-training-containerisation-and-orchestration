<!-- {::options parse_block_html="true" /}-->
Helm Charts - exercise 05
=========================

[[_TOC_]]

Orchestration and Helm
----------------------

This directory contains a list of examples that show how to author [Helm](https://helm.sh/) charts and to use Helm charts to deploy applications.  Examples can be found in  the `./ex05` directory.


These examples are best explored using [Minikube](https://minikube.sigs.k8s.io/docs/start/), with an [Ingress Controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) deployed.  The easiest way to do this (for Linux and MacOS users) is to use https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube , which will ensure that Minikube is deployed with the necessary features and the correct commandline tools are in place (such as `helm` and `kubectl`).

The objective of these examples are to walk through creating and using a Helm chart to deploy an application.  We will use `helm` to generate a skeleton chart, and then we will modify this to mount specific configuration and data so that we have a new and improved `echoserver`.

It is important to remember that Helm is purely a templating solution for generating Kubernetes resource manifests.  The templating language is [Go-templates](https://helm.sh/docs/chart_template_guide/getting_started/).  The high level aim is describe the resources that need to be deployed, and to then be able to customise them at deployment time by passing in a variable set of values (see the `values.yaml` file).  This separation of concerns is fundamental to making charts reusable.

When charts are generated, the resources are sorted by type, and then name before being applied to Kubernetes.  This is an entirely flat structure that also applies to any sub-charts that are recursively included into the main chart - the whole lot are flattened and sorted.  This ensures that runtime dependencies like `Service` related variables, `ConfigMap`s, `Secret`s, `PersistentVolume`s are available to `Deployment`s before the `Deployment` is processed.

Creating a Chart
----------------

Create the directory that will contain the chart components.  There is a convention of holding all chart definitions of a project in the `charts` sub-directory.
```
$ mkdir -p charts/echoserver
```

Use the `helm` command line tool to create a boilerplate chart:
```
$ helm create charts/echoserver
Creating charts/echoserver
$ find charts/echoserver
charts/echoserver
charts/echoserver/charts
charts/echoserver/values.yaml
charts/echoserver/.helmignore
charts/echoserver/templates
charts/echoserver/templates/deployment.yaml
charts/echoserver/templates/NOTES.txt
charts/echoserver/templates/tests
charts/echoserver/templates/tests/test-connection.yaml
charts/echoserver/templates/ingress.yaml
charts/echoserver/templates/_helpers.tpl
charts/echoserver/templates/serviceaccount.yaml
charts/echoserver/templates/hpa.yaml
charts/echoserver/templates/service.yaml
charts/echoserver/Chart.yaml
```
There are a number of key elements that have been generated for us here:
* `charts/echoserver/Chart.yaml` - the manifest file that contains configuration and metadata for the chart
* `charts/echoserver/values.yaml` - contains the default set of values within the chart
* `charts/echoserver/templates` - directory containing the resource templates that are rendered by the go-template engine to produce the finished resource manifest to be passed to Kubernetes
* `charts/echoserver/templates/tests` - user defined `Pod` manifests that are executed on chart install to smoke test the deployed Kubernetes resources
* `charts/echoserver/templates/_helpers.tpl` - template helpers that expose custom reusable go-template components to be used in any of the resource templates
* `charts/echoserver/templates/NOTES.txt` - is a text template that the `helm` command will use to print out summary details when the chart install has completed

This boilerplate chart that we have generated is based on [NGINX](https://www.nginx.com), which we will adjust to create an enhanced `echoserver` example.

Introducing `ConfigMap`s
------------------------

For the new and improved version of the echo server, we will reconfigure NGINX to print out client and server information.  To do this, we need to provide an alternative `.conf` file, and mount a custom `index.html` file in the web server root.


<details>
<summary>Copy the included <code>ConfigMap</code> definitions into <code>charts/echoserver/templates/configmap.yaml</code> to provide two files that will be mounted into the deployed <code>Pod</code>s.  These will be the <code>hello.conf</code> NGINX configuration file, and the <code>index.html</code> file that will server our echo.</summary>
Code is available in [./charts/echoserver/templates/configmap.yaml](./charts/echoserver/templates/configmap.yaml):
<!-- {::options parse_block_html="true" /}-->

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: config-{{ include "echoserver.fullname" . }}
data:
  hello.conf: |
    server {
        listen 80 default_server;
        server_name app_server;

        root /usr/share/nginx/html;
        error_log /var/log/nginx/app-server-error.log notice;
        index index.html;
        expires -1;

        sub_filter_once off;
        sub_filter 'server_hostname' '$hostname';
        sub_filter 'server_address'  '$server_addr:$server_port';
        sub_filter 'server_url'      '$request_uri';
        sub_filter 'remote_addr'     '$remote_addr:$remote_port';
        sub_filter 'server_date'     '$time_local';
        sub_filter 'client_browser'  '$http_user_agent';
        sub_filter 'request_id'      '$request_id';
        sub_filter 'nginx_version'   '$nginx_version';
        sub_filter 'document_root'   '$document_root';
        sub_filter 'proxied_for_ip'  '$http_x_forwarded_for';
    }

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: data-{{ include "echoserver.fullname" . }}
data:
  index.html: |
    <!DOCTYPE html>
    <html>
        <head>
            <title>Hello World - echoserver</title>
            <style>
                body {
                    margin: 0px;
                    font: 20px 'RobotoRegular', Arial, sans-serif;
                    font-weight: 100;
                    height: 100%;
                    color: #0f1419;
                }
                div.info {
                    display: table;
                    background: #e8eaec;
                    padding: 20px 20px 20px 20px;
                    border: 1px dashed black;
                    border-radius: 10px;
                    margin: 0px auto auto auto;
                }
                div.info p {
                    display: table-row;
                    margin: 5px auto auto auto;
                }
                div.info p span {
                    display: table-cell;
                    padding: 10px;
                }
                img {
                    width: 176px;
                    margin: 36px auto 36px auto;
                    display:block;
                }
                div.smaller p span {
                    color: #3D5266;
                }
                h1, h2 {
                    font-weight: 100;
                }
                div.check {
                    padding: 0px 0px 0px 0px;
                    display: table;
                    margin: 36px auto auto auto;
                    font: 12px 'RobotoRegular', Arial, sans-serif;
                }
                #footer {
                    position: fixed;
                    bottom: 36px;
                    width: 100%;
                }
                #center {
                    width: 400px;
                    margin: 0 auto;
                    font: 12px Courier;
                }
            </style>

            <script>
                var ref;
                function checkRefresh() {
                    if (document.cookie == "refresh=1") {
                        document.getElementById("check").checked = true;
                        ref = setTimeout(function(){location.reload();}, 1000);
                    } else {
                    }
                }
                function changeCookie() {
                    if (document.getElementById("check").checked) {
                        document.cookie = "refresh=1";
                        ref = setTimeout(function(){location.reload();}, 1000);
                    } else {
                        document.cookie = "refresh=0";
                        clearTimeout(ref);
                    }
                }
            </script>
        </head>

        <body onload="checkRefresh();">
            <br/><br/>
            <div class="info">
                <p><span>Server name:</span> <span>server_hostname</span></p>
                <p><span>Server address:</span> <span>server_address</span></p>
                <p class="smaller"><span>User Agent:</span> <span>client_browser</span></p>
                <p class="smaller"><span>URI:</span> <span>server_url</span></p>
                <p class="smaller"><span>Doc Root:</span> <span>document_root</span></p>
                <p class="smaller"><span>Date:</span> <span>server_date</span></p>
                <p class="smaller"><span>NGINX Frontend Load Balancer IP:</span> <span>remote_addr</span></p>
                <p class="smaller"><span>Client IP:</span> <span>proxied_for_ip</span></p>
                <p class="smaller"><span>NGINX Version:</span> <span>nginx_version</span></p>
            </div>
            <div class="check">
                <input type="checkbox" id="check" onchange="changeCookie()"> Auto Refresh</input>
            </div>
            <div id="footer">
                <div id="center" align="center">
                    Request ID: request_id
                </div>
            </div>
        </body>
    </html>

```
</details>

In the above snippet, you will see a notation like:
```
name: data-{{ include "echoserver.fullname" . }}
```
This illustrates the [go-templating syntax](https://helm.sh/docs/chart_template_guide/getting_started/) where the **moustaches** `{{ ... }}` indcate where templating language is inserted.  The reference to `include "echoserver.fullname" .` shows that the template defined in `_helpers.tpl` for `echoserver.fullname` has been included at this point and the current variable context (scope) referenced by the `.` has been passed into the helper template.  This is how the included templates variable space gets populated with things like `.Chart.Name`, forinstance.


Mounting Volumes
----------------

Now that we have added `ConfigMap`s to our chart containing config and data files that we need to pass into the running containers, we now need to map them to ```volumes``` and ```volumeMounts```.


<details>
<summary>
Modify the <code>spec:</code> section of the  <code>charts/echoserver/templates/deployment.yaml</code> template as follows, so that the <code>ConfigMap</code>s are mounted as volumes into the running containers.</summary>
Code is available in [./charts/echoserver/templates/deployment.yaml](./charts/echoserver/templates/deployment.yaml):
<!-- {::options parse_block_html="true" /}-->

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "echoserver.fullname" . }}
  labels:
    {{- include "echoserver.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "echoserver.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "echoserver.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "echoserver.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          volumeMounts:
            - name: data-{{ include "echoserver.fullname" . }}
              mountPath: /usr/share/nginx/html
            - name: config-{{ include "echoserver.fullname" . }}
              mountPath: /etc/nginx/conf.d
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: config-{{ include "echoserver.fullname" . }}
          configMap:
            name: config-{{ include "echoserver.fullname" . }}
        - name: data-{{ include "echoserver.fullname" . }}
          configMap:
            name: data-{{ include "echoserver.fullname" . }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}

```
</details>


Custom Values
-------------

Values can be `--set` individually or passed in using a `--values` file.  Save the following configuration into a local `values.yaml` file so that we can use it later.
```yaml
---
replicaCount: 3
ingress:
  enabled: true
  className: ""
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
  hosts:
    - paths:
        - path: /ping
          pathType: Prefix
  tls: []

```

This configuration will turn on the `Ingress` and overide the exposure to respond on all host names at the `/ping` URI, and increase the number of replicas to 3.


Generating the Resource Manifest
--------------------------------

It is useful for debug purposes to review the resource manifest that Helm generates before actually applying it to Kubernetes.  This can be done with `helm template`.  Run the following to check that the chart generates as expected:
```console
# sepcify the NAME of the deployment and the chart along with our custom values.yaml file
$ helm template test charts/echoserver --values values.yaml
...
```

If any templating errors are thrown then you can try adding the `--debug` switch to get some more details.  The most common problems are schema errors, and variable scoping issues.

Install the chart
-----------------

```console
$ helm upgrade --install test charts/echoserver --values values.yaml
Release "test" does not exist. Installing it now.
NAME: test
LAST DEPLOYED: Fri Jun 25 01:21:07 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  http:///ping
  # note the URL has not host portion as the Ingress configuration has omited this
```

Inspect the deployed chart release:
```console
$ helm list
NAME	NAMESPACE	REVISION	UPDATED                                 	STATUS  	CHART           	APP VERSION
test	default  	1       	2021-06-25 01:39:43.265550832 +1200 NZST	deployed	echoserver-0.1.0	1.16.0
$ helm status test
NAME: test
LAST DEPLOYED: Fri Jun 25 01:39:43 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  http:///ping
```

Explore the contents of the deployment with `helm get` and `helm history`.


Check that the `Pod`s have come up running and the expected resources have been created:
```console
$ kubectl get all,configmap,ingress
NAME                                   READY   STATUS    RESTARTS   AGE
pod/test-echoserver-7bbb7f8547-8p97z   1/1     Running   0          3s
pod/test-echoserver-7bbb7f8547-8v79z   1/1     Running   0          3s
pod/test-echoserver-7bbb7f8547-tdb8z   0/1     Running   0          3s

NAME                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/kubernetes        ClusterIP   10.96.0.1       <none>        443/TCP   3h33m
service/test-echoserver   ClusterIP   10.99.218.148   <none>        80/TCP    3s

NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/test-echoserver   2/3     3            2           3s

NAME                                         DESIRED   CURRENT   READY   AGE
replicaset.apps/test-echoserver-7bbb7f8547   3         3         2       3s

NAME                               DATA   AGE
configmap/config-test-echoserver   1      3s
configmap/data-test-echoserver     1      3s
configmap/kube-root-ca.crt         1      3h32m

NAME                                        CLASS    HOSTS   ADDRESS   PORTS   AGE
ingress.networking.k8s.io/test-echoserver   <none>   *                 80      3s
```


Now, check that the `Ingress` has correctly exposed the web service:
```console
$ curl http://$(minikube ip)/ping
<!DOCTYPE html>
<html>
    <head>
        <title>Hello World - echoserver</title>
...
```

And if you point your web browser at http://`$(minikube ip)`/ping , you should see something like the following:

![Test Echo Server in browser](test-echoserver.png "Test Echo Server")


Experiment with upgrades
------------------------

We can adjust the deployed Helm Chart release by rerunning the `helm upgrade`, and passing in different values.
Lets adjust the deployed `Pod`s by varying the `replicaCount`:
```
$ helm upgrade --install test charts/echoserver --values values.yaml --set replicaCount=0
```
Check the deployed resources, and you will see that there are no `Pod`s:
```
$ kubectl get all
NAME                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/kubernetes        ClusterIP   10.96.0.1       <none>        443/TCP   20h
service/test-echoserver   ClusterIP   10.99.218.148   <none>        80/TCP    16h

NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/test-echoserver   0/0     0            0           16h

NAME                                         DESIRED   CURRENT   READY   AGE
replicaset.apps/test-echoserver-7bbb7f8547   0         0         0       16h
```
Refresh your browser and you will see that the application has become unavailable with an HTTP `503` error.
Adjust the replicas again to 1:
```
$ helm upgrade --install test charts/echoserver --values values.yaml --set replicaCount=1
```
Keep refreshing the browser and you will see the application return, and that the `Server name` does not change (because we only have one instance).
Adjust the replicas again to 5:
```
$ helm upgrade --install test charts/echoserver --values values.yaml --set replicaCount=5
```
Keep refreshing the browser and you will see the `Server name` will cycle through the list of available `Pod`s as they spin up.

The same principles apply if you want to deploy a modified version of the chart - Helm and Kubernetes will reconcile changes to deployed resources and the container images using whatever update strategy has been specified - `Recreate`, or `RollingUpdate` - https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy.


Cleaning up
-----------

The chart deployment is removed with:
```console
$ helm uninstall test
release "test" uninstalled
```


More on Go Templating
=====================

For the following examples. create a new empty chart with:
```
$ mkdir -p charts/syntax
$ helm create charts/syntax
$ rm -rf charts/syntax/templates/*
```

From here, we will add code into the `charts/syntax/templates` directory and then check our work with:
```
$ helm template test charts/syntax
```


Standard/Built-In Objects
-------------------------

There are a number of built-in objects available within the template context that have useful values for scripting the resource manifests (described here https://helm.sh/docs/chart_template_guide/builtin_objects/).  These are:
* Release - information about the release instance to be deployed to Kubernetes including Name, Namespace, Revision
* Chart - the chart metadata including Name and Version
* Capabilities - information about the Kubernetes cluster versions and API support
* Template - the current template Name and BasePath
* Values - a map object that points to all the values defined in the `values.yaml` file and any other configuration passed at `helm` runtime

Create a a template `charts/syntax/templates/configmap.yaml` with the following contents:

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: mychart-configmap
data:
  theChartName: "{{ .Chart.Name }}"
```

Now template the charts with:
```console
$ helm template test charts/syntax
---
# Source: syntax/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mychart-configmap
data:
  theChartName: "syntax"
```

Conditionals
------------

Flow control is managed with an `if/elseif/else/end` construct (https://helm.sh/docs/chart_template_guide/control_structures/#ifelse).  This is often used to wrap entire templates when determining whether to include a complete resource in the manifest or not. eg:
```yaml
{{ if .Values.enabled }}
apiVersion: v1
kind: ConfigMap
...
{{ end }}
```
With the above, you now get no output:
```
$ helm template test charts/syntax
```

Add the following to `charts/syntax/templates/configmap.yaml`:
```yaml
  amIEnabled: "{{ if .Values.enabled }}true{{else}}false{{ end }}"
```

Render the template alternately with:
```console
$ helm template test charts/syntax # gives:   amIEnabled: "false"
# and
$ helm template test charts/syntax --set enabled=true # gives:   amIEnabled: "true"
```

The full list of conditional operators can be found here https://helm.sh/docs/chart_template_guide/functions_and_pipelines/#operators-are-functions.


Functions
---------

There are many functions available (https://helm.sh/docs/chart_template_guide/functions_and_pipelines/) that do all sorts of interesting things including formating YAML, and Base64, as well as quoting strings, defaulting values.  Functions can be piped together much like UNIX commands.

Add the following example to set a default value if the variable isn't set and then quote the output:
```yaml
  myVar: {{ .Values.myVar | default "we didnt set it" | quote }}
```

When we render this we get:
```console
$ helm template test charts/syntax
#   myVar: "we didnt set it"
$ helm template test charts/syntax --set enabled=true --set myVar="this text"
#   myVar: "this text"
```

Loops
-----

Looping constructs are possible with the `range` construct (https://helm.sh/docs/chart_template_guide/control_structures/#looping-with-the-range-action).  This typically works be taking an iterable array or map, and assigning the elements to locally scoped variables (denoted with the `$`).

Add the following to `charts/syntax/templates/configmap.yaml`, and render the output.
```yaml
{{- range $key, $value := .Release }}
  {{ $key }}: {{ $value | quote }}
{{- end }}
```

Render the `.Release` map values into the chart with:
```console
$ helm template test charts/syntax --set enabled=true
...
  IsInstall: "true"
  IsUpgrade: "false"
  Name: "test"
  Namespace: "default"
  Revision: "1"
  Service: "Helm"
...
```

Variable Context and Scope
--------------------------

What is happening with variable scope is often a bit confusing with go-templates.  This can be to do with understanding what is in the current context within different functions and sections of a template.

The `.` context is the beginning of a reference to an element relative to the current scope. So, in the open context of a template, the built-ins are available like `.Release` and `.Values`.  However, the availability changes as the context changes, and this is especially aparent with a `with` statement or a `range` statement.

Observe the following:
```yaml
  ReleaseNameIs: {{ .Release.Name }} # correct
  {{ with .Release }}
  WithReleaseNameIs: {{ .Name }} # correct
  Chart: {{ .Chart.Name }} # wrong - will throw an error !!!
  ProperChart: {{ $.Chart.Name }} # correct
  {{ end }}
```
`.Name` is available as it is a member of `.Release`.  `.Chart.Name` is not available because the relative context is `.Release`, however if we prefix with `$`, then we address the root context and can access `$.Chart.Name`.

Like wise, in a `range`:
```yaml
{{- range $key, $value := .Release }}
  {{ printf "%s_%s" $.Chart.Name $key }}: {{ $value | quote }} # correct
  # {{ printf "%s_%s" .Chart.Name $key }}: {{ $value | quote }}  # wrong - will throw an error !!!
{{- end }}
```

You can assign an outer scope to a variable so that it is addressable in the inner scope:
```yaml
{{- $myval := "xyz" }} # assign a value to a outer scoped variable
{{- range $key, $value := .Release }}
{{- with $value }}
# {{ printf "%s_%s" $myval . | quote }} # correct - we can reference inner and outer
{{- end }}
{{- end }}
```

A Note on Whitespace
--------------------

Template commands can generate whitespace depending on the circumstances.  If the code is all on one line such as:
```yaml
  amIEnabled: "{{ if .Values.enabled }}true{{else}}false{{ end }}"
```
then no additional spacing is inserted.  However, if as with control structures, the markup is split over multiple lines then this will introduce whitespace unless specifically treated.
This is done by introducing the `-` to the `{{}}` curly braces.  If it is added left `{{- }}` then left whitespace will be chomped, likewise if it is added right `{{ -}}` then right whitespace will be chomped.  Consider this:
```yaml
{{- range $key, $value := .Release }}
  {{ $key }}: {{ $value | quote }}
{{- end }}
```
this will remove leading whitespace from each line in the `range` keeping the lines tight together.

Now this:
```yaml
{{ range $key, $value := .Release }}
  {{ $key }}: {{ $value | quote }}
{{ end }}
```
we get an extra newline infront of each line making appear double spaced.


Wrapping up
-----------

In this section we have covered the basics of creating and deploying Helm charts, and we have also had a look at some of the key behaviours of the Helm flavour of the go-templating language.

[Home &#x3e;&#x3e;](../README.md)
