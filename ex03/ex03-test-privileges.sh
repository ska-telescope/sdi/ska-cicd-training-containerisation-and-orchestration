#!/bin/sh
NAME=ex03-test-pscap
TAG=0.0.1

echo "Remove the running ${NAME} container if it exists"
docker inspect ${NAME} >/dev/null 2>&1 && docker rm -f ${NAME}

echo ""
echo ""
echo "Build the container image with the pscap tools"
cat <<EOF | docker build -t ${NAME}:${TAG} -
FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install libcap-ng-utils iputils-ping curl -y && \
    apt-get clean -y
EOF

echo ""
echo ""
echo "Run the container with pscap and show me the default capabilities:"
# This will show chown, dac_override, fowner, fsetid, kill,
# setgid, setuid, setpcap, net_bind_service, net_raw, sys_chroot,
#  mknod, audit_write, setfcap
echo "pscap -a " | \
docker run --rm -i -a stdin -a stdout -a stderr ${NAME}:${TAG} bash

echo ""
echo ""
echo "Now drop all privileges and show what we have?: **NOTHING**"
# This will show we have none
echo "pscap -a " | \
docker run --rm --security-opt=no-new-privileges --cap-drop ALL \
   -i -a stdin -a stdout -a stderr ${NAME}:${TAG} bash

echo ""
echo ""
echo "Now show that we can chain containers together like Unix pipes, with different security capabilities:"
# The first container has no privileges
# The second container will run as the user who executed docker run
#  including having the same uid, gid, and home directory
cat /etc/hosts | \
docker run --rm --security-opt=no-new-privileges \
   --cap-drop ALL -i -a stdin -a stdout -a stderr \
   ${NAME}:${TAG} grep localhost | \
docker run --rm -i -a stdin -a stdout -a stderr \
   -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro \
   --user=$(id -u) -v ${HOME}:${HOME} -w ${HOME} -e HOME=${HOME} \
   ${NAME}:${TAG} awk '{print $1}'
