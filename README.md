Containerisation and Orchestration
==================================

Repository of examples to follow introduction to Containerisation and Orchestration.

Clone this repository with:
```console
$ git clone https://gitlab.com/piersharding/containerisation-and-orchestration.git
```

Each directory has a self contained example to work through, with a corresponding README.md file outlining the process and objectives, eg:

```console
$ cd containerisation-and-orchestration
$ cd ex01
$ ls
clean.sh  ex01-launch-an-echo.sh  README.md
```
Requirements
============

All these examples were developed on an Ubuntu 20.04 desktop, using Docker 20.10.7, and Kubernetes 1.21.1.  Most of these examples should work out of the box on any other platform with the exception of [play back an MP3 file](./ex03-fun-with-containers.sh) in [exploring run time controls for containers - ex03](./ex03).

Conventions
===========

I regularly use http://0.0.0.0 as a test URL that points to the local Minikube ingress controller.  This works on Linux, but perhaps does not work for you so the 0.0.0.0 address can be substituted for your local machines IP (not localhost!), or just run `minikube ip` and record what IP address `Minikube` says it is running on and then use this instead.


Exercises
=========

* [basic usage of containers in Docker - ex01](./ex01)
* [the process of creating your own OCI images - ex02](./ex02)
* [exploring run time controls for containers - ex03](./ex03)
* [Introduction to Orchestration - ex04](./ex04)
* [Orchestration and Helm - ex05](./ex05)
